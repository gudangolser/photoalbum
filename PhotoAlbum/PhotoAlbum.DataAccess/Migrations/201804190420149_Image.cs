namespace PhotoAlbum.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Image : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ImageEntities",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Image = c.Binary(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.PhotoEntities", "Image_Id", c => c.Long());
            CreateIndex("dbo.PhotoEntities", "Image_Id");
            AddForeignKey("dbo.PhotoEntities", "Image_Id", "dbo.ImageEntities", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PhotoEntities", "Image_Id", "dbo.ImageEntities");
            DropIndex("dbo.PhotoEntities", new[] { "Image_Id" });
            DropColumn("dbo.PhotoEntities", "Image_Id");
            DropTable("dbo.ImageEntities");
        }
    }
}