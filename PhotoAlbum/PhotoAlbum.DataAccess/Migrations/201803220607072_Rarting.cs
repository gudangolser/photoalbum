namespace PhotoAlbum.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Rarting : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PhotoEntities", "Rating", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PhotoEntities", "Rating");
        }
    }
}
