namespace PhotoAlbum.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class PhotoNameOfFile : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PhotoEntities", "NameOfFile", c => c.String());
            DropColumn("dbo.PhotoEntities", "Path");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PhotoEntities", "Path", c => c.String());
            DropColumn("dbo.PhotoEntities", "NameOfFile");
        }
    }
}
