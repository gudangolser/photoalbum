namespace PhotoAlbum.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PhotoEntities",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        DateTimeCreate = c.DateTime(nullable: false),
                        Comment = c.String(),
                        Name = c.String(),
                        Owner_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserEntities", t => t.Owner_Id)
                .Index(t => t.Owner_Id);
            
            CreateTable(
                "dbo.UserEntities",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Login = c.String(),
                        Password = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RateEntities",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Rate = c.Int(nullable: false),
                        Photo_Id = c.Long(),
                        Voter_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PhotoEntities", t => t.Photo_Id)
                .ForeignKey("dbo.UserEntities", t => t.Voter_Id)
                .Index(t => t.Photo_Id)
                .Index(t => t.Voter_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RateEntities", "Voter_Id", "dbo.UserEntities");
            DropForeignKey("dbo.RateEntities", "Photo_Id", "dbo.PhotoEntities");
            DropForeignKey("dbo.PhotoEntities", "Owner_Id", "dbo.UserEntities");
            DropIndex("dbo.RateEntities", new[] { "Voter_Id" });
            DropIndex("dbo.RateEntities", new[] { "Photo_Id" });
            DropIndex("dbo.PhotoEntities", new[] { "Owner_Id" });
            DropTable("dbo.RateEntities");
            DropTable("dbo.UserEntities");
            DropTable("dbo.PhotoEntities");
        }
    }
}
