namespace PhotoAlbum.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class PhotoPath : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PhotoEntities", "Path", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PhotoEntities", "Path");
        }
    }
}
