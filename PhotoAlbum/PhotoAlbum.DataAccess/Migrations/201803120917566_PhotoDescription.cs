namespace PhotoAlbum.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PhotoDescription : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PhotoEntities", "Description", c => c.String());
            DropColumn("dbo.PhotoEntities", "Comment");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PhotoEntities", "Comment", c => c.String());
            DropColumn("dbo.PhotoEntities", "Description");
        }
    }
}
