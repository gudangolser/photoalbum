namespace PhotoAlbum.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Rating : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PhotoEntities", "Rating", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PhotoEntities", "Rating", c => c.Int(nullable: false));
        }
    }
}
