namespace PhotoAlbum.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LoggingExceptions2 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.ExceptionEntities");
            AlterColumn("dbo.ExceptionEntities", "Id", c => c.Long(nullable: false, identity: true));
            AddPrimaryKey("dbo.ExceptionEntities", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.ExceptionEntities");
            AlterColumn("dbo.ExceptionEntities", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.ExceptionEntities", "Id");
        }
    }
}
