namespace PhotoAlbum.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;
    using Models;
    using Contexts;
    using Microsoft.AspNet.Identity;

    internal sealed class Configuration : DbMigrationsConfiguration<DatabaseContext>
    {
        public const string AdminName = "SuperPowerUser";

        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DatabaseContext context)
        {
            //if (System.Diagnostics.Debugger.IsAttached == false)
            //    System.Diagnostics.Debugger.Launch();

            var manager = new UserManager<UserEntity, long>(new CustomUserStore(new DatabaseContext()));

            var supaUser = manager.FindByName("SuperPowerUser");
            if (supaUser == null)
            {
                supaUser = new UserEntity
                {
                    UserName = "SuperPowerUser",
                    Email = "taiseer.joudeh@mymail.com",
                    EmailConfirmed = true,
                    FirstName = "Taiseer",
                    LastName = "Joudeh"
                };

                manager.Create(supaUser, "MySuperP@ssword!");
            }

            context.SaveChanges();
        }
    }
}
