﻿using System;

namespace PhotoAlbum.DataAccess.Models
{
    public class PhotoEntity
    {
        public long Id { get; set; }
        public DateTime DateTimeCreate { get; set; }
        public DateTime DateTimeUpdate { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public virtual UserEntity Owner { get; set; }
        public int? Rating { get; set; }
        public virtual ImageEntity Image { get; set; }
    }
}
