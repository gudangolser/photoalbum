﻿using System;

namespace PhotoAlbum.DataAccess.Models
{
    public class ExceptionEntity
    {
            public long Id { get; set; }
            public string ExceptionMessage { get; set; }    // message about exception
            public string ControllerName { get; set; }  // controller where exception appeared
            public string ActionName { get; set; }  // action where exception appeared
            public string StackTrace { get; set; }  // stack trace
            public DateTime Date { get; set; }  // date and time of exception

    }
}
