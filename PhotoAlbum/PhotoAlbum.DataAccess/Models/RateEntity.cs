﻿namespace PhotoAlbum.DataAccess.Models
{
    public class RateEntity
    {
        public long Id { get; set; }
        public virtual UserEntity Voter { get; set; }
        public virtual PhotoEntity Photo { get; set; }
        public int Rate { get; set; }
    }
}
