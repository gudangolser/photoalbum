﻿namespace PhotoAlbum.DataAccess.Models
{
    public class ImageEntity
    {
        public long Id { get; set; }
        public byte[] Image { get; set; }
    }
}
