﻿using System.Data.Entity;
using PhotoAlbum.DataAccess.Models;
using PhotoAlbum.DataAccess.Interfaces;
using Microsoft.AspNet.Identity.EntityFramework;

namespace PhotoAlbum.DataAccess.Contexts
{
    public class DatabaseContext : IdentityDbContext<UserEntity, CustomRole,
    long, CustomUserLogin, CustomUserRole, CustomUserClaim>, IDatabaseContext
    {
        public DbSet<PhotoEntity> PhotoEntities { get; set; }
        public DbSet<RateEntity> RateEntities { get; set; }
        public DbSet<ExceptionEntity> ExceptionEntities { get; set; }
        public DbSet<RefreshTokenEntity> RefreshTokenEntities { get; set; }
        public DbSet<ImageEntity> ImageEntities { get; set; }

        public DatabaseContext()
            : base("DatabaseContext")
        {
            Configuration.ProxyCreationEnabled = true;
            Configuration.LazyLoadingEnabled = true;
        }
        
    }
}
