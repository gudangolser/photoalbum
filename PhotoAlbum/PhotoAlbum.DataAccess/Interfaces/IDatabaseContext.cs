﻿using PhotoAlbum.DataAccess.Models;
using System;
using System.Data.Entity;
using System.Threading.Tasks;

namespace PhotoAlbum.DataAccess.Interfaces
{
    public interface IDatabaseContext : IDisposable
    {
        DbSet<PhotoEntity> PhotoEntities { get; set; }
        DbSet<RateEntity> RateEntities { get; set; }
        DbSet<ExceptionEntity> ExceptionEntities { get; set; }
        DbSet<RefreshTokenEntity> RefreshTokenEntities { get; set; }
        DbSet<ImageEntity> ImageEntities { get; set; }

        Task<int> SaveChangesAsync();
        int SaveChanges();
    }
}
