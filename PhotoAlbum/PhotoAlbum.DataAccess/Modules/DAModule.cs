﻿using Autofac;
using PhotoAlbum.DataAccess.Contexts;
using PhotoAlbum.DataAccess.Interfaces;

namespace PhotoAlbum.DataAccess.Modules
{
    public class DAModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<DatabaseContext>().As<IDatabaseContext>().InstancePerRequest();
        }
    }
}
