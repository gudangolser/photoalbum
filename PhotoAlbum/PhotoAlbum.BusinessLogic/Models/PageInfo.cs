﻿namespace PhotoAlbum.BusinessLogic.Models
{
    public class PageInfo
    {
        public int TypeOfOrder { get; set; }
        public int PageNumber { get; set; }
        public const int PageSize = 5;
    }
}
