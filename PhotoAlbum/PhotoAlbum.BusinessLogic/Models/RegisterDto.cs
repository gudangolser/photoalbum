﻿using System.ComponentModel.DataAnnotations;

namespace PhotoAlbum.BusinessLogic.Models
{
    public class RegisterDto
    {
        [Required]
        public string Login { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "Passwords don't equall")]
        [DataType(DataType.Password)]
        public string PasswordConfirm { get; set; }
    }
}
