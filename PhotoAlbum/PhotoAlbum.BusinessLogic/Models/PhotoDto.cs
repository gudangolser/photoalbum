﻿using System;

namespace PhotoAlbum.BusinessLogic.Models
{
    public class PhotoDto
    {
        public long Id { get; set; }
        public DateTime DateTimeCreate { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string Owner { get; set; }
        public int? Rating { get; set; }
        public long ImageId { get; set; }
    }
}
