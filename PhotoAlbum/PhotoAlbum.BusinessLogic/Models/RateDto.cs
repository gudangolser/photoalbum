﻿namespace PhotoAlbum.BusinessLogic.Models
{
    public class RateDto
    {
        public long PhotoId { get; set; }
        public int Rate { get; set; }
    }
}
