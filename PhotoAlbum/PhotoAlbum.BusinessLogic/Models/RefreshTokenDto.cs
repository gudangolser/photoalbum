﻿using System;

namespace PhotoAlbum.BusinessLogic.Models
{
    public class RefreshTokenDto
    {
        public long Id { get; set; }
        public string Subject { get; set; }
        public DateTime IssuedUtc { get; set; }
        public DateTime ExpiresUtc { get; set; }
        public string ProtectedTicket { get; set; }
        public string TokenHash { get; set; }
    }
}
