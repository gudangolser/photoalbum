﻿using System.ComponentModel.DataAnnotations;

namespace PhotoAlbum.BusinessLogic.Models
{
    public class NewPassDto
    {
        [Required]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Required]
        [Compare("NewPassword", ErrorMessage = "Passwords don't equall")]
        [DataType(DataType.Password)]
        public string PasswordConfirm { get; set; }
    }
}
