﻿using Autofac;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using PhotoAlbum.BusinessLogic.Infrastructure;
using PhotoAlbum.BusinessLogic.Interfaces;
using PhotoAlbum.BusinessLogic.Services;
using PhotoAlbum.DataAccess.Interfaces;
using PhotoAlbum.DataAccess.Models;
using System.Web;

namespace PhotoAlbum.BusinessLogic.Modules
{
    public class BLModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PhotoAlbumService>().As<IPhotoAlbumService>().InstancePerRequest();
            builder.RegisterType<UserService>().As<IUserService>().InstancePerRequest();
            builder.RegisterType<AuthService>().As<IAuthService>().InstancePerRequest();
            builder.RegisterType<ApplicationUserManager>();
            builder.Register(c => new CustomUserStore(c.Resolve<IDatabaseContext>())).AsImplementedInterfaces().InstancePerRequest();
            builder.Register(c => HttpContext.Current.GetOwinContext().Authentication).As<IAuthenticationManager>();
            builder.Register(c => new IdentityFactoryOptions<ApplicationUserManager>
            {
                DataProtectionProvider = new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("Application​")
            });
        }
    }
}




