﻿using PhotoAlbum.BusinessLogic.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoAlbum.BusinessLogic.Interfaces
{
    public interface IPhotoAlbumService
    {
        Task<List<PhotoDto>> GetAllPhotosListAsync(PageInfo pageInfo);
        Task<List<PhotoDto>> GetPhotosListAsync(string username, PageInfo pageInfo);
        Task<int> GetCountOfPagesAsync();
        Task<int> GetCountOfPagesAsync(string username);
        Task<int> RatePhotoAsync(long id, int rate);
        Task<long> SavePhotoAsync(PhotoDto photo);
        Task<long> EditPhotoAsync(long id, PhotoDto photo);
        Task<long> DeletePhotoAsync(long id);
        Task<long> UploadFileIntoDatabase(byte[] file);
        Task<PhotoDto> GetPhotoAsync(long id);
        Task<byte[]> GetImageAsync(long id);
        bool UserIsOwner(string username);
    }
}
