﻿using PhotoAlbum.BusinessLogic.Models;
using System;
using System.Threading.Tasks;

namespace PhotoAlbum.BusinessLogic.Interfaces
{
    public interface IAuthService : IDisposable
    {
        Task<bool> AddRefreshToken(RefreshTokenDto token);
        Task<RefreshTokenDto> FindRefreshToken(string tokenHash);
        Task<RefreshTokenDto> FindUsersRefreshToken(string username);
        Task<bool> RemoveRefreshToken(string tokenHash);
    }
}