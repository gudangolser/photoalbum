﻿using PhotoAlbum.BusinessLogic.Models;

namespace PhotoAlbum.BusinessLogic.Interfaces
{
    public interface IUserContext
    {
        UserInfo GetUser();
    }
}
