﻿using PhotoAlbum.BusinessLogic.Models;
using System.Threading.Tasks;

namespace PhotoAlbum.BusinessLogic.Interfaces
{
    public interface IUserService
    {
        Task<UserDto> GetInfoAboutUserAsync();
        Task<long> SaveInfoAboutUserAsync(UserDto userInfo);
        Task<string> GetNameAsync(string username);
        Task<long> ChangePassAsync(NewPassDto userInfo);
        bool UserIsOwner(string name);
    }
}
