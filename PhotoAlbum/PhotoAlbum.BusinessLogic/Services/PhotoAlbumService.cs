﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PhotoAlbum.BusinessLogic.Models;
using PhotoAlbum.DataAccess.Interfaces;
using PhotoAlbum.DataAccess.Models;
using PhotoAlbum.BusinessLogic.Interfaces;
using PhotoAlbum.BusinessLogic.Infrastructure;
using System;
using PhotoAlbum.BusinessLogic.Enums;
using System.Net;
using System.Transactions;
using System.Data.Entity;
using AutoMapper;
using AutoMapper.QueryableExtensions;

namespace PhotoAlbum.BusinessLogic.Services
{
    public class PhotoAlbumService : IPhotoAlbumService
    {
        private readonly IDatabaseContext db;
        private readonly ApplicationUserManager userManager;
        private readonly IUserContext userContext;

        public PhotoAlbumService(IDatabaseContext context, ApplicationUserManager appUserManager, IUserContext userContext)
        {
            db = context;
            userManager = appUserManager;
            this.userContext = userContext;
        }

        public async Task<List<PhotoDto>> GetAllPhotosListAsync(PageInfo pageInfo)
        {
            return await Task.Run(() =>
            {
                var photoDtos = Select(Load(pageInfo));
                return photoDtos;
            });
        }

        public async Task<List<PhotoDto>> GetPhotosListAsync(string username, PageInfo pageInfo)
        {
            var user = await userManager.FindByNameAsync(username);
            if(user != null)
            {
                var userPhotoDtos = Select(Load(user, pageInfo));
                return userPhotoDtos;
            }
            throw new Exception($"User with login {username} does not exist");
        }

        private List<PhotoDto> Select(IQueryable<PhotoEntity> list)
        {
            var userPhotoDtos = list.ProjectTo<PhotoDto>().ToList();
            return userPhotoDtos;
        }

        private IQueryable<PhotoEntity> Load(PageInfo pageInfo)
        {
            var photoEntities = db.PhotoEntities;
            var list = SortAndCut(photoEntities, pageInfo);
            return list;
        }
        private IQueryable<PhotoEntity> Load(UserEntity user, PageInfo pageInfo)
        {
            var photoEntities = db.PhotoEntities
                                        .Where(x => x.Owner.UserName == user.UserName);
            return SortAndCut(photoEntities, pageInfo);
        }
        public async Task<long> SavePhotoAsync(PhotoDto photo)
        {
            var user = userContext.GetUser();
            if (user != null)
            {
                var photoEntity = Mapper.Map<PhotoDto, PhotoEntity>(photo);

                photoEntity.DateTimeCreate = DateTime.UtcNow;
                photoEntity.DateTimeUpdate = photoEntity.DateTimeCreate;
                photoEntity.Owner = await userManager.FindByNameAsync(user.Name);
                photoEntity.Image = await db.ImageEntities.FirstOrDefaultAsync(x => x.Id == photo.ImageId);
                db.PhotoEntities.Add(photoEntity);
                await db.SaveChangesAsync();

                return photoEntity.Id;
            }
            throw new Exception();
        }

        public async Task<int> RatePhotoAsync(long id, int rate)
        {
            var principalUser = userContext.GetUser();
            if (principalUser != null)
            {
                var photo = db.PhotoEntities.FirstOrDefault(x => x.Id == id);
                if (photo != null)
                {
                    int sumRate;
                    var user = await userManager.FindByNameAsync(principalUser.Name);

                    var rateEntity = db.RateEntities
                        .FirstOrDefault(x => x.Voter.UserName == user.UserName && x.Photo.Id == id);
                    var creatMode = (rateEntity == null);
                    if (creatMode)
                    {
                        rateEntity = new RateEntity
                        {
                            Voter = user,
                            Photo = photo
                        };
                    }

                    rateEntity.Rate = rate;

                    using (TransactionScope transaction = new TransactionScope())
                    {
                        if (creatMode)
                        {
                            db.RateEntities.Add(rateEntity);
                        }

                        db.SaveChanges();

                        var rates = db.RateEntities.Where(x => x.Photo.Id == id)
                            .Select(x => x.Rate).ToList();
                        var count = rates.Count;
                        var sum = rates.Sum();
                        var rating = Math.Floor((double) (sum / count));

                        photo.Rating = (int) rating;
                        db.SaveChanges();
                        sumRate = (int) rating;
                        transaction.Complete();
                    }

                    return sumRate;
                }

                throw new BusinessLogicException("photo not found", HttpStatusCode.MethodNotAllowed);
            }

            throw new Exception();
        }

        public async Task<int> GetCountOfPagesAsync()
        {
            return await Task.Run(() =>
            {
                var count = db.PhotoEntities.Count();
                var pageSize = PageInfo.PageSize;

                return (int)Math.Ceiling((decimal)count / pageSize);
            });
        }

        public async Task<int> GetCountOfPagesAsync(string username)
        {
            return await Task.Run(() =>
            {
                var count = db.PhotoEntities.Count(x => x.Owner.UserName == username);
                var pageSize = PageInfo.PageSize;

                return (int)Math.Ceiling((decimal)count / pageSize);
            });
        }

        public async Task<long> DeletePhotoAsync(long id)
        {
            var user = userContext.GetUser();
            if (user != null)
            {
                return await Task.Run(() =>
                {
                    long returnedId = 0;
                    var photoEntity = db.PhotoEntities.FirstOrDefault(x => x.Id == id);
                    if (photoEntity != null)
                    {
                        if (photoEntity.Owner.UserName != user.Name)
                        {
                            throw new UnauthorizedAccessException("You don't have permitions delete this photo");
                        }

                        using (TransactionScope transaction = new TransactionScope())
                        {
                            var rates = db.RateEntities.Where(x => x.Photo.Id == id);
                            foreach (var rate in rates)
                            {
                                db.RateEntities.Remove(rate);
                            }

                            db.SaveChanges();
                            var image = photoEntity.Image;
                            db.PhotoEntities.Remove(photoEntity);
                            db.SaveChanges();

                            db.ImageEntities.Remove(image);
                            db.SaveChanges();

                            returnedId = id;
                            transaction.Complete();
                        }
                    }

                    return returnedId;
                });
            }

            throw new Exception();
        }

        public async Task<long> EditPhotoAsync(long id, PhotoDto photo)
        {
            var user = userContext.GetUser();
            if (user != null)
            {
                long returnedId = 0;
                var photoEntity = db.PhotoEntities.FirstOrDefault(x => x.Id == id);

                if (photoEntity != null)
                {
                    if (photoEntity.Owner.UserName != user.Name)
                    {
                        throw new UnauthorizedAccessException("You don't have permitions edit this photo");
                    }

                    Mapper.Map(photo, photoEntity);
                    photoEntity.DateTimeUpdate = DateTime.UtcNow;
                    returnedId = photoEntity.Id;
                }

                await db.SaveChangesAsync();
                return returnedId;
            }
            throw new Exception();
        }

        public async Task<PhotoDto> GetPhotoAsync(long id)
        {
            var user = userContext.GetUser();
            if (user != null)
            {
                return await Task.Run(() =>
                {
                    var photoEntity = db.PhotoEntities.FirstOrDefault(x => x.Id == id && x.Owner.UserName == user.Name);
                    if (photoEntity != null)
                    {
                        var photo = Mapper.Map<PhotoEntity, PhotoDto>(photoEntity);
                        return photo;
                    }
                    throw new BusinessLogicException("photo not found",HttpStatusCode.NotFound);
                });
            }

            throw new Exception();
        }

        private IQueryable<PhotoEntity> SortAndCut(IQueryable<PhotoEntity> photoEntities, PageInfo pageInfo)
        {
            var page = pageInfo.PageNumber;
            var pageSize = PageInfo.PageSize;

            if (pageInfo.TypeOfOrder == (int)PhotoOrder.ByDate)
            {
                photoEntities = photoEntities.OrderByDescending(t => t.DateTimeCreate);
            }
            else
            {
                photoEntities = photoEntities.OrderByDescending(t => t.Rating);
            }

            return photoEntities.Skip((page - 1) * pageSize).Take(pageSize);
        }

        public async Task<long> UploadFileIntoDatabase(byte[] file)
        {
            var image = new ImageEntity
            {
                Image = file
            };
            db.ImageEntities.Add(image);
            await db.SaveChangesAsync();
            return image.Id;
        }

        public async Task<byte[]> GetImageAsync(long id)
        {
            return await Task.Run(() =>
            {
                return db.PhotoEntities.Where(x => x.Id == id).Select(x => x.Image.Image)
                    .FirstOrDefault();
            });
        }

        public bool UserIsOwner(string name)
        {
            return userContext.GetUser()?.Name == name;
        }
    }
}
