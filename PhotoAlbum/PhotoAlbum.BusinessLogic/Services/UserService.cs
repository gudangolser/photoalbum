﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using PhotoAlbum.BusinessLogic.Infrastructure;
using PhotoAlbum.BusinessLogic.Interfaces;
using PhotoAlbum.BusinessLogic.Models;
using PhotoAlbum.DataAccess.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoAlbum.BusinessLogic.Services
{
    public class UserService : IUserService
    {
        private readonly ApplicationUserManager userManager;
        private readonly IUserContext userContext;

        public UserService(ApplicationUserManager appUserManager, IUserContext userContext)
        {
            userManager = appUserManager;
            this.userContext = userContext;
        }

        public async Task<long> ChangePassAsync(NewPassDto userInfo)
        {
            var user = userContext.GetUser();
            if (user != null)
            {
                var userEntity = await userManager.FindByNameAsync(user.Name);
                var result =
                    await userManager.ChangePasswordAsync(userEntity.Id, userInfo.OldPassword, userInfo.NewPassword);
                if (result.Succeeded)
                {
                    userEntity.NumberOfPassChanges++;
                    await userManager.UpdateAsync(userEntity);
                    return userEntity.Id;
                }

                throw new ValidationException("Changing of password failed: " + String.Join(" ", result.Errors.ToList()));
            }

            throw new Exception();
        }

        public async Task<UserDto> GetInfoAboutUserAsync()
        {
            var principalUser = userContext.GetUser();
            if (principalUser != null)
            {
                var userEntity = await userManager.FindByNameAsync(principalUser.Name);
                if (userEntity != null)
                {
                    var user = Mapper.Map<UserEntity, UserDto>(userEntity);
                    return user;
                }
                throw new BusinessLogicException("user not found");
            }

            throw new Exception();
        }

        public async Task<string> GetNameAsync(string username)
        {
            var user = await userManager.FindByNameAsync(username);
            if (user != null)
            {
                return user.FirstName;
            }

            throw new Exception($"User with login {username} does not exist");
        }

        public async Task<long> SaveInfoAboutUserAsync(UserDto userInfo)
        {
            var principalUser = userContext.GetUser();
            if (principalUser != null && principalUser.Name == userInfo.Login)
            {
                var user = await userManager.FindByIdAsync(userInfo.Id);
                if (user != null)
                {
                    Mapper.Map(userInfo, user);
                    IdentityResult result = await userManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {
                        return user.Id;
                    }

                    throw new ValidationException("Update failed: " + String.Join(" ", result.Errors.ToList()));
                }

                throw new BusinessLogicException("user not found");
            }

            throw new Exception();
        }

        public bool UserIsOwner(string name)
        {
            return userContext.GetUser()?.Name == name;
        }
    }
}
