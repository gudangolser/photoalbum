﻿using Autofac.Util;
using AutoMapper;
using PhotoAlbum.BusinessLogic.Interfaces;
using PhotoAlbum.BusinessLogic.Models;
using PhotoAlbum.DataAccess.Interfaces;
using PhotoAlbum.DataAccess.Models;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoAlbum.BusinessLogic.Services
{
    class AuthService : Disposable, IAuthService
    {
        private readonly IDatabaseContext db;

        public AuthService(IDatabaseContext context)
        {
            db = context;
        }

        public async Task<bool> AddRefreshToken(RefreshTokenDto token)
        {
            var existingToken = db.RefreshTokenEntities.SingleOrDefault(r => r.Subject == token.Subject);

            if (existingToken != null)
            {
                await RemoveRefreshToken(existingToken);
            }
            var refreshToken = Mapper.Map<RefreshTokenDto, RefreshTokenEntity>(token);
            db.RefreshTokenEntities.Add(refreshToken);

            return await db.SaveChangesAsync() > 0;
        }

        public async Task<bool> RemoveRefreshToken(string tokenHash)
        {
            var refreshToken = db.RefreshTokenEntities.SingleOrDefault(r => r.TokenHash == tokenHash);

            if (refreshToken != null)
            {
                db.RefreshTokenEntities.Remove(refreshToken);
                return await db.SaveChangesAsync() > 0;
            }
            return false;
        }

        public async Task<bool> RemoveRefreshToken(RefreshTokenEntity refreshToken)
        {
            db.RefreshTokenEntities.Remove(refreshToken);
            return await db.SaveChangesAsync() > 0;
        }

        public async Task<RefreshTokenDto> FindRefreshToken(string tokenHash)
        {
            var refreshTokenEntity = db.RefreshTokenEntities.SingleOrDefault(r => r.TokenHash == tokenHash);
            return await Task.Run(() =>
            {
                if (refreshTokenEntity != null)
                {
                    var refreshToken = Mapper.Map<RefreshTokenEntity, RefreshTokenDto>(refreshTokenEntity);
                    return refreshToken;
                }
                return null;
            });
        }

        public async Task<RefreshTokenDto> FindUsersRefreshToken(string username)
        {
            var refreshTokenEntity = db.RefreshTokenEntities.SingleOrDefault(r => r.Subject == username);
            return await Task.Run(() =>
            {
                if (refreshTokenEntity != null)
                {
                    var refreshToken = Mapper.Map<RefreshTokenEntity, RefreshTokenDto>(refreshTokenEntity);
                    return refreshToken;
                }
                return null;
            });
        }
    }
}
