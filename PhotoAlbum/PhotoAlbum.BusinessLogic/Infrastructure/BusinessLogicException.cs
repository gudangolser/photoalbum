﻿using System;
using System.Net;

namespace PhotoAlbum.BusinessLogic.Infrastructure
{
    public class BusinessLogicException : ApplicationException
    {
        public HttpStatusCode StatusCode { get; set; }

        public BusinessLogicException() 
            :base() { }

        public BusinessLogicException(string message, HttpStatusCode status = HttpStatusCode.InternalServerError)
            :base(message)
        {
            StatusCode = status;
        }
    }
}
