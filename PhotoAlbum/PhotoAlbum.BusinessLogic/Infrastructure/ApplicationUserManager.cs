﻿using Microsoft.AspNet.Identity;
using PhotoAlbum.DataAccess.Models;
using Microsoft.AspNet.Identity.Owin;

namespace PhotoAlbum.BusinessLogic.Infrastructure
{
    public class ApplicationUserManager : UserManager<UserEntity, long>
    {
        public ApplicationUserManager(IUserStore<UserEntity, long> store, IdentityFactoryOptions<ApplicationUserManager> options)
                : base(store)
        {
            UserValidator = new UserValidator<UserEntity, long>(this)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };
            // Configure validation logic for passwords 
            PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };             
            
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                UserTokenProvider =
                    new DataProtectorTokenProvider<UserEntity, long>(
                        dataProtectionProvider.Create("ASP.NET Identity"));
            }
        }
    }
}