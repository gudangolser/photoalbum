﻿using System.Net;
using System.Web.Http.ModelBinding;

namespace PhotoAlbum.BusinessLogic.Infrastructure
{
    public class ValidationException : BusinessLogicException
    {
        public ValidationException(string message)
            : base(message, HttpStatusCode.BadRequest) { }

        public ValidationException(ModelStateDictionary modelState)
            : base(modelState.GetStateError(), HttpStatusCode.BadRequest) { }
    }
}
