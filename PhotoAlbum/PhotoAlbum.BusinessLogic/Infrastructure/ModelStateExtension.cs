﻿using System.Linq;
using System.Text;
using System.Web.Http.ModelBinding;

namespace PhotoAlbum.BusinessLogic.Infrastructure
{
    public static class ModelStateExtension
    {
        public static string GetStateError(this ModelStateDictionary modelState)
        {
            if (!modelState.IsValid)
            {
                var sb = new StringBuilder();

                var messages = modelState.Values.ToList();

                for (int i = 0; i < modelState.Count; i++)
                {
                    var error = messages[i].Errors.FirstOrDefault();
                    if (error != null)
                        sb.AppendLine(error.ErrorMessage);
                }

                return sb.ToString();
            }

            return string.Empty;
        }
    }
}
