﻿namespace PhotoAlbum.BusinessLogic.Enums
{
    enum PhotoOrder
    {
        ByDate=1,
        ByPopularity=2
    }
}
