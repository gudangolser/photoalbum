﻿using Autofac.Integration.WebApi;
using PhotoAlbum.BusinessLogic.Infrastructure;
using PhotoAlbum.DataAccess.Interfaces;
using PhotoAlbum.DataAccess.Models;
using PhotoAlbum.WebApi.Utils;
using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;

namespace PhotoAlbum.WebApi.Filters
{
    public class ExceptionLoggerFilter : IAutofacExceptionFilter
    {
        private readonly IDatabaseContext db;
        public ExceptionLoggerFilter(IDatabaseContext context)
        {
            db = context;
        }
        public async Task OnExceptionAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
        {
            if (actionExecutedContext.Exception != null)
            {
                if (actionExecutedContext.Exception is ValidationException)
                {
                    actionExecutedContext.Response = actionExecutedContext.Request
                        .CreateErrorResponse(HttpStatusCode.BadRequest, actionExecutedContext.Exception.Message);
                }
                else if (actionExecutedContext.Exception is BusinessLogicException)
                {
                    var blEx = (BusinessLogicException)actionExecutedContext.Exception;
                    var jsonResult = new JsonResult(true, blEx.Message, blEx.StatusCode);
                    actionExecutedContext.Response = actionExecutedContext.Request
                        .CreateResponse(jsonResult);
                }
                else if (actionExecutedContext.Exception is UnauthorizedAccessException)
                {
                    actionExecutedContext.Response = actionExecutedContext.Request
                        .CreateErrorResponse(HttpStatusCode.Forbidden, actionExecutedContext.Exception.Message);
                }
                else
                {
                    actionExecutedContext.Response = actionExecutedContext.Request
                        .CreateErrorResponse(HttpStatusCode.InternalServerError, actionExecutedContext.Exception.Message);
                }

                ExceptionEntity ex = new ExceptionEntity()
                {
                    ExceptionMessage = actionExecutedContext.Exception.Message,
                    StackTrace = actionExecutedContext.Exception.StackTrace,
                    ControllerName = actionExecutedContext.ActionContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                    ActionName = actionExecutedContext.ActionContext.ActionDescriptor.ActionName,
                    Date = DateTime.Now
                };
                db.ExceptionEntities.Add(ex);
                await db.SaveChangesAsync();
            }
        }
    }
}