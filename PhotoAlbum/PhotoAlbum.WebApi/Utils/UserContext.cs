﻿using PhotoAlbum.BusinessLogic.Interfaces;
using System.Threading;
using PhotoAlbum.BusinessLogic.Models;

namespace PhotoAlbum.WebApi.Utils
{
    public class UserContext : IUserContext
    {
        public UserInfo GetUser()
        {
            var name = Thread.CurrentPrincipal.Identity.Name;
            if (name != null)
            {
                var user = new UserInfo {Name = name};
                return user;
            }
            return null;
        }
    }
}