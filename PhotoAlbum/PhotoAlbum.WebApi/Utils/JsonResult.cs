﻿using PhotoAlbum.BusinessLogic.Infrastructure;
using System.Collections.Generic;
using System.Net;
using System.Web.Http.ModelBinding;

namespace PhotoAlbum.WebApi.Utils
{
    public class JsonResult
    {
        public object Model { get; set; }
        public string Message { get; set; }
        public List<string> MessageDetails { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public bool HasError { get; set; }

        public JsonResult(object model, string message = null)
        {
            Model = model;
            Message = message;
            StatusCode = HttpStatusCode.OK;
        }

        public JsonResult(ModelStateDictionary modelState)
        {
            StatusCode = HttpStatusCode.BadRequest;
            HasError = !modelState.IsValid;
            Message = modelState.GetStateError();
        }

        public JsonResult(bool hasError, string errorMessage, HttpStatusCode status = HttpStatusCode.InternalServerError)
        {
            StatusCode = status;
            HasError = hasError;
            Message = errorMessage;
        }

        public JsonResult(bool hasError,
                          string errorMessage,
                          List<string> messageDetails,
                          HttpStatusCode status = HttpStatusCode.InternalServerError)
            : this(hasError, errorMessage, status)
        {
            MessageDetails = messageDetails;
        }
    }
}
