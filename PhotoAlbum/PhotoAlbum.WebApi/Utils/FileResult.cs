﻿using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace PhotoAlbum.WebApi.Utils
{
    public class FileResult : IHttpActionResult
    {
        private readonly byte[] fileBytes;
        private readonly string contentType;

        public FileResult(byte[] fileBytes, string contentType)
        {
            this.fileBytes = fileBytes;
            this.contentType = contentType;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(fileBytes)
            };
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(contentType);

            return Task.FromResult(response);
        }
    }
}