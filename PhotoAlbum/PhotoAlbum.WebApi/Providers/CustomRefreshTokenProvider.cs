﻿using Microsoft.Owin.Security.Infrastructure;
using System;
using System.Threading.Tasks;
using PhotoAlbum.BusinessLogic.Interfaces;
using Microsoft.AspNet.Identity.Owin;
using PhotoAlbum.BusinessLogic.Models;
using PhotoAlbum.BusinessLogic.Infrastructure;

namespace PhotoAlbum.WebApi.Providers
{
    public class CustomRefreshTokenProvider : IAuthenticationTokenProvider
    {
        public async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            var refreshTokenId = Guid.NewGuid().ToString("n");
            var authService = context.OwinContext.Get<IAuthService>();
            
            var refreshTokenLifeTime = "43200";//month
            var token = new RefreshTokenDto()
            {
                TokenHash = Helper.GetHash(refreshTokenId),
                Subject = context.Ticket.Identity.Name,
                IssuedUtc = DateTime.UtcNow,
                ExpiresUtc = DateTime.UtcNow.AddMinutes(Convert.ToDouble(refreshTokenLifeTime))
            };

            context.Ticket.Properties.IssuedUtc = token.IssuedUtc;
            context.Ticket.Properties.ExpiresUtc = token.ExpiresUtc;

            token.ProtectedTicket = context.SerializeTicket();

            var result = await authService.AddRefreshToken(token);

            if (result)
            {
                context.SetToken(refreshTokenId);
            }
        }

        public void Create(AuthenticationTokenCreateContext context)
        {
            throw new NotImplementedException();
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            throw new NotImplementedException();
        }

        public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            string hashedTokenId = Helper.GetHash(context.Token);
            var authService = context.OwinContext.Get<IAuthService>();

            var refreshToken = await authService.FindRefreshToken(hashedTokenId);

            if (refreshToken != null)
            {
                //Get protectedTicket from refreshToken class
                context.DeserializeTicket(refreshToken.ProtectedTicket);
                await authService.RemoveRefreshToken(hashedTokenId);
            }
        }
    }
}