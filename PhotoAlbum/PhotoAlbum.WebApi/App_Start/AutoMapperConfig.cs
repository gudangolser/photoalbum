﻿using AutoMapper;
using PhotoAlbum.BusinessLogic.Models;
using PhotoAlbum.DataAccess.Models;

namespace PhotoAlbum.WebApi
{
    public class AutoMapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
                                {
                                    cfg.CreateMap<PhotoDto, PhotoEntity>()
                                      .ForMember(dest => dest.Owner, opt => opt.Ignore())
                                      .ForMember(dest => dest.DateTimeCreate, opt => opt.Ignore())
                                      .ForMember(dest => dest.DateTimeUpdate, opt => opt.Ignore())
                                      .ForMember(dest => dest.Image, opt => opt.Ignore());
                                    cfg.CreateMap<PhotoEntity, PhotoDto>()
                                      .ForMember(dest => dest.Owner, opt => opt.MapFrom(x => x.Owner.UserName))
                                      .ForMember(dest => dest.ImageId, opt => opt.Ignore());
                                    cfg.CreateMap<UserEntity, UserDto>()
                                      .ForMember(dest => dest.Login, opt => opt.MapFrom(x => x.UserName));
                                    cfg.CreateMap<UserDto, UserEntity>()
                                      .ForMember(dest => dest.UserName, opt => opt.Ignore());
                                    cfg.CreateMap<RegisterDto, UserEntity>()
                                      .ForMember(dest => dest.Email, opt => opt.MapFrom(x => x.Email))
                                      .ForMember(dest => dest.FirstName, opt => opt.MapFrom(x => x.FirstName))
                                      .ForMember(dest => dest.LastName, opt => opt.MapFrom(x => x.LastName))
                                      .ForMember(dest => dest.UserName, opt => opt.MapFrom(x => x.Login))
                                      .ForAllOtherMembers(x => x.Ignore());
                                    cfg.CreateMap<RefreshTokenEntity, RefreshTokenDto>();
                                    cfg.CreateMap<RefreshTokenDto, RefreshTokenEntity>();
                                });
        }
    }
}