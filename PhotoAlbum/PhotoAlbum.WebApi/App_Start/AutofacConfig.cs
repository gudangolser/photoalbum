﻿using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using PhotoAlbum.BusinessLogic.Modules;
using PhotoAlbum.DataAccess.Modules;
using PhotoAlbum.WebApi.Modules;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;

namespace PhotoAlbum.WebApi
{
    public class AutofacConfig
    {
        public IContainer ConfigureContainer(HttpConfiguration config)
        {
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterWebApiFilterProvider(config);
            builder.RegisterModule(new BLModule());
            builder.RegisterModule(new DAModule());
            builder.RegisterModule(new ApiModule());

            var container = builder.Build();

            var resolver = new AutofacWebApiDependencyResolver(container);

            // Configure Web API with the dependency resolver.
            GlobalConfiguration.Configuration.DependencyResolver = resolver;
            config.DependencyResolver = resolver;
            var mvcResolver = new AutofacDependencyResolver(container);
            DependencyResolver.SetResolver(mvcResolver);
            return container;
        }
    }
}