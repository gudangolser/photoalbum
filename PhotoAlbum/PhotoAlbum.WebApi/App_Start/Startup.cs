﻿using Microsoft.Owin;
using Owin;
using System;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Owin.Security.Jwt;
using System.Web.Http;
using System.Web.Routing;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using System.Configuration;
using PhotoAlbum.WebApi.Utils;
using PhotoAlbum.BusinessLogic.Infrastructure;
using PhotoAlbum.BusinessLogic.Interfaces;
using PhotoAlbum.WebApi.Providers;
using System.Text;

[assembly: OwinStartup(typeof(PhotoAlbum.WebApi.Startup))]

namespace PhotoAlbum.WebApi
{
    public class Startup
    {

        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            var container = new AutofacConfig().ConfigureContainer(config);

            app.UseAutofacMvc();

            AreaRegistration.RegisterAllAreas();
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            RouteConfig.RegisterRoutes(RouteTable.Routes);
            JsonConfig.Configure(config);
            AutoMapperConfig.Initialize();

            app.CreatePerOwinContext(() => DependencyResolver.Current.GetService<ApplicationUserManager>());
            app.CreatePerOwinContext(() => DependencyResolver.Current.GetService<IAuthService>());

            ConfigureOAuth(app);
            app.UseAutofacWebApi(config);
            app.UseAutofacMiddleware(container);

            ConfigureWebApi(app, config);
        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            var issuer = ConfigurationManager.AppSettings["issuer"];
            var secret = Encoding.UTF8.GetBytes(ConfigurationManager.AppSettings["secret"]);
            app.UseJwtBearerAuthentication(new JwtBearerAuthenticationOptions
            {
                AuthenticationMode = AuthenticationMode.Active,
                AllowedAudiences = new[] { "Any" },
                IssuerSecurityKeyProviders = new IIssuerSecurityKeyProvider[]
                {
                    new SymmetricKeyIssuerSecurityKeyProvider(issuer, secret)
                }
            });

            app.UseOAuthAuthorizationServer(new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true,//only for development mode
                TokenEndpointPath = new PathString("/oauth2/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(3),
                Provider = new CustomOAuthProvider(),
                RefreshTokenProvider = new CustomRefreshTokenProvider(),
                AccessTokenFormat = new CustomJwtFormat(issuer)
            });
        }

        public void ConfigureWebApi(IAppBuilder app, HttpConfiguration config)
        {
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Authentication configs
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            app.UseWebApi(config);
        }
    }
}