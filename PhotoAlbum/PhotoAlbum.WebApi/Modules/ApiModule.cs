﻿using Autofac;
using Autofac.Integration.WebApi;
using PhotoAlbum.BusinessLogic.Interfaces;
using PhotoAlbum.DataAccess.Interfaces;
using PhotoAlbum.WebApi.Controllers;
using PhotoAlbum.WebApi.Filters;
using PhotoAlbum.WebApi.Utils;

namespace PhotoAlbum.WebApi.Modules
{
    public class ApiModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UserContext>().As<IUserContext>().InstancePerRequest();
            builder.Register(c => new ExceptionLoggerFilter(c.Resolve<IDatabaseContext>())).AsWebApiExceptionFilterFor<PhotosController>().InstancePerRequest();
            builder.Register(c => new ExceptionLoggerFilter(c.Resolve<IDatabaseContext>())).AsWebApiExceptionFilterFor<UserController>().InstancePerRequest();
            builder.Register(c => new ExceptionLoggerFilter(c.Resolve<IDatabaseContext>())).AsWebApiExceptionFilterFor<AccountController>().InstancePerRequest();
        }
    }
}
