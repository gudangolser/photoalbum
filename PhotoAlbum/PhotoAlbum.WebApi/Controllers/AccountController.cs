﻿using PhotoAlbum.BusinessLogic.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using System.Web.Http;
using PhotoAlbum.BusinessLogic.Models;
using PhotoAlbum.DataAccess.Models;
using PhotoAlbum.BusinessLogic.Interfaces;
using PhotoAlbum.WebApi.Utils;
using System;
using AutoMapper;

namespace PhotoAlbum.WebApi.Controllers
{
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private readonly ApplicationUserManager userManager;
        private readonly IAuthService authService;

        public AccountController(ApplicationUserManager userManager, IAuthService authService)
        {
            this.userManager = userManager;
            this.authService = authService;
        }

        [AllowAnonymous]
        [Route("Register")]
        [HttpPost]
        public async Task<JsonResult> Register(RegisterDto model)
        {
            if (ModelState.IsValid)
            {
                UserEntity user = Mapper.Map<RegisterDto, UserEntity>(model);
                
                IdentityResult result = await userManager.CreateAsync(user, model.Password);
                if(result.Succeeded)
                {
                    return new JsonResult(result.Succeeded);
                }
                throw new ValidationException("Registration failed: " + String.Join(" ", result.Errors.ToList()));
            }
            throw new ValidationException(ModelState);
        }

        [Authorize]
        [Route("Logout")]
        [HttpPost]
        public async Task<JsonResult> Logout()
        {
            var currentUserName = User.Identity.Name;
            var token = await authService.FindUsersRefreshToken(currentUserName);
            if(token != null)
            {
                 return new JsonResult(await authService.RemoveRefreshToken(token.TokenHash));
            }
            return new JsonResult(true);
        }
    }
}
