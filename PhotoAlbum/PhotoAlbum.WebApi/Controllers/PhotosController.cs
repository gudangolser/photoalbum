﻿using System.Web.Http;
using PhotoAlbum.BusinessLogic.Interfaces;
using PhotoAlbum.BusinessLogic.Models;
using System.Threading.Tasks;
using System;
using PhotoAlbum.WebApi.Utils;

namespace PhotoAlbum.WebApi.Controllers
{
    [RoutePrefix("api/Photos")]
    public class PhotosController : ApiController
    {
        private readonly IPhotoAlbumService photoAlbumService;

        public PhotosController(IPhotoAlbumService service)
        {
            photoAlbumService = service;
        }

        [Route("List")]
        [HttpGet]
        public async Task<JsonResult> List([FromUri] PageInfo pageInfo)
        {
            var list = await photoAlbumService.GetAllPhotosListAsync(pageInfo);
            return new JsonResult(list);
        }

        [Route("{username}")]
        [HttpGet]
        public async Task<JsonResult> List([FromUri]string username, [FromUri] PageInfo pageInfo)
        {
            return new JsonResult(await photoAlbumService.GetPhotosListAsync(username, pageInfo));
        }

        [Authorize]
        [Route("{username}/Manage")]
        [HttpGet]
        public async Task<JsonResult> Manage([FromUri]string username, [FromUri] PageInfo pageInfo)
        {
            if (photoAlbumService.UserIsOwner(username))
            {
                return new JsonResult(await photoAlbumService.GetPhotosListAsync(username, pageInfo));
            }
            throw new UnauthorizedAccessException();
        }

        [Authorize]
        [Route("GetPhoto/{id}")]
        [HttpGet]
        public async Task<JsonResult> GetPhoto([FromUri]long id)
        {
            return new JsonResult(await photoAlbumService.GetPhotoAsync(id));
        }

        [Route("GetImageFromDatabase/{id}")]
        [HttpGet]
        public async Task<FileResult> GetImageFromDatabase([FromUri]long id)
        {
            var img = await photoAlbumService.GetImageAsync(id);
            return new FileResult(img, "image/png");
        }

        [Authorize]
        [Route("Rate/{id}/{rate}")]
        [HttpPost]
        public async Task<JsonResult> Rate([FromUri]long id, int rate)
        {
            var jsonResult = new JsonResult(await photoAlbumService.RatePhotoAsync(id, rate));
            return jsonResult;
        }

        [Authorize]
        [Route("Edit/{id}")]
        [HttpPut]
        public async Task<JsonResult> Edit([FromUri]long id, [FromBody]PhotoDto photo)
        {
            return new JsonResult(await photoAlbumService.EditPhotoAsync(id, photo));
        }

        [Authorize]
        [Route("Delete/{id}")]
        [HttpDelete]
        public async Task<JsonResult> Delete([FromUri]long id)
        {
            return new JsonResult(await photoAlbumService.DeletePhotoAsync(id));
        }

        [Authorize]
        [Route("SavePhoto")] 
        [HttpPost]
        public async Task<JsonResult> SavePhoto(PhotoDto photo)
        {
            return new JsonResult(await photoAlbumService.SavePhotoAsync(photo));
        }

        [Authorize]
        [HttpPost]
        [Route("UploadFileIntoDatabase")]
        public async Task<JsonResult> UploadFileIntoDatabase()
        {
            var file = await Request.Content.ReadAsByteArrayAsync();
            return new JsonResult(await photoAlbumService.UploadFileIntoDatabase(file));
        }

        [HttpGet]
        [Route("GetCountOfPages")]
        public async Task<JsonResult> GetCountOfPages()
        {
            return new JsonResult(await photoAlbumService.GetCountOfPagesAsync());
        }

        [HttpGet]
        [Route("GetCountOfPages")]
        public async Task<JsonResult> GetCountOfPages([FromUri]string username)
        {
            return new JsonResult(await photoAlbumService.GetCountOfPagesAsync(username));
        }
    }
}
