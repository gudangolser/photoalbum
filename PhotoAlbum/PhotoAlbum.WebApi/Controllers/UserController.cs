﻿using PhotoAlbum.BusinessLogic.Infrastructure;
using PhotoAlbum.BusinessLogic.Interfaces;
using PhotoAlbum.BusinessLogic.Models;
using PhotoAlbum.WebApi.Utils;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace PhotoAlbum.WebApi.Controllers
{
    [RoutePrefix("api/User")]
    public class UserController : ApiController
    {
        private readonly IUserService userService;
        public UserController(IUserService service)
        {
            userService = service;
        }

        [Authorize]
        [Route("{username}/GetInfo")]
        [HttpGet]
        public async Task<JsonResult> GetInfoAboutUser([FromUri]string username)
        {
            if(userService.UserIsOwner(username))
            {
                return new JsonResult(await userService.GetInfoAboutUserAsync());
            }
            throw new UnauthorizedAccessException();
        }

        [Authorize]
        [Route("SaveInfo")]
        [HttpPut]
        public async Task<JsonResult> SaveInfoAboutUser([FromBody]UserDto userInfo)
        {
            return new JsonResult(await userService.SaveInfoAboutUserAsync(userInfo));
        }

        [Authorize]
        [Route("ChangePass")]
        [HttpPut]
        public async Task<JsonResult> ChangePass([FromBody]NewPassDto userInfo)
        {
            if (ModelState.IsValid)
            {
                return new JsonResult(await userService.ChangePassAsync(userInfo));
            }
            throw new ValidationException(ModelState);
        }

        [Route("{username}/GetName")]
        [HttpGet]
        public async Task<JsonResult> GetName([FromUri]string username)
        {
            return new JsonResult(await userService.GetNameAsync(username));
        }
    }
}
