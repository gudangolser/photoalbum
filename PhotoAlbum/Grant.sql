﻿IF NOT EXISTS (SELECT name FROM sys.server_principals WHERE name = 'IIS APPPOOL\PhotoServer')
BEGIN
    CREATE LOGIN [IIS APPPOOL\PhotoServer] 
      FROM WINDOWS WITH DEFAULT_DATABASE=[master], 
      DEFAULT_LANGUAGE=[us_english]
END
GO
CREATE USER [PhotoAlbumUser] 
  FOR LOGIN [IIS APPPOOL\PhotoServer]
GO
EXEC sp_addrolemember 'db_owner', 'PhotoAlbumUser'
GO