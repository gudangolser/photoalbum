export class ProfileModel {
    id: string;
    login: string;
    email: string;
    firstName: string;
    lastName: string;
  }