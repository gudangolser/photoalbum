export class ChangePassModel {
    oldPassword: string;
    newPassword: string;
    passwordConfirm: string;
}