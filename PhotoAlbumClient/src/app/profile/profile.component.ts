import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { HttpService } from '../services/http.service';
import { ProfileModel } from './models/profile.model';
import { environment } from '../../environments/environment';
import { ChangePassModel } from './models/change-pass.model';
import { AuthService } from '../services/authentication.service';

@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  private  baseUrl = environment.baseUrl;
  public model: ProfileModel;
  public passModel: ChangePassModel;
  public success: boolean;
  private userName: string;
  public changePassword: boolean;

  constructor(private http: HttpService, private router: ActivatedRoute, private navigator: Router, private authService: AuthService) {
    this.passModel = new ChangePassModel();
  }

  ngOnInit() {
    this.router.params.subscribe(params => {
      this.userName = params['username'];
      if(this.userName) {
        this.http.get<ProfileModel>("api/User/"+this.userName+"/GetInfo")
          .subscribe(result=>{
              this.model = result.model;
          }, error => console.error(error));
      }
    });
  }

  public confirm() {
    var id;
    this.http
    .put<number>("api/User/SaveInfo",this.model)
    .subscribe(result=>{
        id = result.model;
    }, error => console.error(error));
  }

  public changePass() {
    var id;
    this.http
    .put<number>("api/User/ChangePass",this.passModel)
    .subscribe(result=>{
      if(result.model) {
        id = result.model;
        this.authService.logout();
      }
    }, error => console.error(error));
  }
}
