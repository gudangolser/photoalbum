import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

import { RegisterModel } from './models/registration.model';
import { HttpService } from './../services/http.service';

@Component({
  selector: 'registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent{
  private  baseUrl = environment.baseUrl;
  public model: RegisterModel;
  public success: boolean;

  constructor(private http: HttpService, private router: ActivatedRoute, private navigator: Router) {
    this.model = new RegisterModel();
  }

  public Login() {
    this.navigator.navigate(['login']);
  }

  public Register() {
   this.http
   .post<boolean>('api/Account/Register',this.model)
   .subscribe(result => {
      this.success = result.model;
      if(this.success) {
        this.navigator.navigate(['login']);
       }
    }, error => console.error(error));
  }
}



