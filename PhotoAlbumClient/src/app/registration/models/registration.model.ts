export class RegisterModel {
    login: string;
    password: string;
    passwordConfirm: string;
    email: string;
    firstName: string;
    lastName: string;
  }