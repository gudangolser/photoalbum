import { Component } from '@angular/core';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css','./../../node_modules/angular-notifier/styles.scss']
})
export class AppComponent {
  title = 'app';
  time = new Date();
}
