import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/authentication.service';

@Injectable()
export class LoginGuard implements CanActivate {
    constructor(private service: AuthService, private navigator: Router ) {
    }

    canActivate() {
        var canActivate = !this.service.checkLogin();
        if(!canActivate) {
             this.navigator.navigate(['/all-photos']);
        }
        return canActivate;
    }
}