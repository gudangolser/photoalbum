import { Component } from '@angular/core';
import { AuthService } from '../services/authentication.service';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {
  submenu: string[] = ["Logout","Profile"];
  selectedItem: string;
  loggedOut: boolean;
  username: string;

  constructor(private authService: AuthService) {
    authService.loggedOut$.subscribe(result=>{
      this.loggedOut = result;
    });
    authService.getUserName$.subscribe(result=>{
      this.username = result;
    });
    authService.checkLogin();
  }
  
  logout() {
    this.authService.logout();
  } 
}