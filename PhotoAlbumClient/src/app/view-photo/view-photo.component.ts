import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';

import { OnClickEvent, OnRatingChangeEven, OnHoverRatingChangeEvent } from 'angular-star-rating';

import { PhotoModel } from '../common/models/photo.model';
import { environment } from '../../environments/environment';
import { HttpService } from '../services/http.service';
import { AddPhotoComponent } from '../add-photo/add-photo.component';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { NotifierService } from 'angular-notifier';
import { AuthService } from '../services/authentication.service';
import { WindowComponent } from '../common/components/confirm-window/confirm-window.component';

@Component({
  selector: 'view-photo',
  templateUrl: './view-photo.component.html',
  styleUrls: ['./view-photo.component.scss']
})
export class ViewPhotoComponent implements OnInit {
  @Input() photo: PhotoModel;
  path: string;
  @Input() changeMode: boolean;
  rate: number;
  @Output() photoChanged = new EventEmitter<boolean>();
 
  constructor(private http: HttpService, private authService: AuthService, 
              private navigator: Router, public dialog: MatDialog, 
              private cookieService: CookieService, private readonly notifier: NotifierService) {
  }

  ngOnInit() {
    this.path = environment.baseUrl+'api/Photos/GetImageFromDatabase/'+this.photo.id;

    if(this.photo.rating == null)
      this.photo.rating = 0;
      var cookie = this.cookieService.get(this.photo.id.toString());
      if(!!cookie){
        this.rate = +cookie;
      }
  }
  
  public openUserPhotos(owner: string) {
    this.navigator.navigate([owner]);
  }

  public editPhoto() {
    let dialogRef = this.dialog.open(AddPhotoComponent, {
      width: '400pt',
      data: {photoId: this.photo.id}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.photoChanged.emit(true);
      }
    });
  }

  public delete() {
    let dialogRef = this.dialog.open(WindowComponent, {
      width: '400pt',
      data: {message: "Are you sure you want to delete this photo?"}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.http
        .delete<number>("api/Photos/Delete/"+this.photo.id)
        .subscribe(result=>{
          if(result.model){
             this.photoChanged.emit(true);
          }
        });
      }
    });
  }

  onClick = ($event:OnClickEvent) => {
    if(this.authService.checkLogin()) {
      var cookie = this.cookieService.get(this.photo.id.toString());
      if(!cookie){
        var rate = $event.rating;
            this.http
            .post<number>("api/Photos/Rate/"+this.photo.id+"/"+rate,null)
            .subscribe(result=>{
              if(result.model){
                this.photo.rating = result.model;
              }
            });
            this.cookieService.put(this.photo.id.toString(),rate.toString());
      }
      else this.notifier.notify("info","You already rated this photo");
    } else {
      this.rate = 0;
      this.notifier.notify("info","You are not authorized. Only authorized users can rate photos");
    }
  };
}
