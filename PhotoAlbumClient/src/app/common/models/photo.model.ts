import { UserModel } from "./user.model";

export class PhotoModel {
    id: number;
    dateTimeCreate: Date;
    description: string;
    name: string;
    owner: string;
    rating: number;
    ImageId: number;
  }