export class JsonResult<T> {
    model: T;
    message: string;
    messageDetails: string[];
    statusCode: number;
    hasError: boolean;
}