import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";

@Component({
    selector: 'paging',
    templateUrl: './paging.component.html',
    styleUrls: ['./paging.component.css']
  })
export class PagingComponent  implements OnInit {
    @Input() pageNumber: number;
    @Input() count: number;
    @Output() newPage = new EventEmitter<number>();
    public pages = [];
    constructor() {
    }
    
    ngOnInit(): void {
      this.changePages();
    } 
    
    ngOnChanges(): void {
      this.changePages()
    }
    
    public selectPage(page: number) {
      this.pageNumber = page;
      this.newPage.emit(page);
    }

    public changePages() {
      var arrOfPages =[];
      if(this.count<=5){
        var len = this.count;
        var startPosition = 1;
      } else {
        var len = 5;
        if(this.pageNumber<=3) {
          var startPosition = 1;
        } else if (this.pageNumber>=this.count-2) {
          var startPosition = this.count-4;//пятый с конца
        } else {
          var startPosition = this.pageNumber-2;
        }
      }

      if(this.count)
        for(var i=startPosition;i<len+startPosition;i++){
          arrOfPages.push(i);
        }
      this.pages = arrOfPages;
    }
}