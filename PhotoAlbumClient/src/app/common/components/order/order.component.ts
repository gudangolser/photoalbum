import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({
    selector: 'order',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.css']
  })
export class OrderComponent {
    @Input() type: number;
    public order  = [{id: "byDate", value: 1, name: "New first"},{id: "byRate",value: 2, name: "Popular first"}];
    @Output() newOrder = new EventEmitter<number>();
    
    public changeOrder() {
      this.newOrder.emit(this.type); 
    }
}