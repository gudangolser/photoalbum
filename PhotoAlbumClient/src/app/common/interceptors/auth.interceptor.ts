import { HttpInterceptor, HttpRequest, HttpHandler, 
         HttpEvent, HttpErrorResponse, HttpSentEvent, 
         HttpHeaderResponse, HttpProgressEvent, HttpResponse, 
         HttpUserEvent } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';
import { Injectable, Injector } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { AuthService } from "../../services/authentication.service";
import { Router } from "@angular/router";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    private authService: AuthService

    constructor(private injector: Injector, private navigator: Router){
        this.authService = this.injector.get(AuthService);
    }
    
    setHeaders(request) {
        return function (token) {
          return request.clone({
            setHeaders: {
              Accept: 'application/json',
              Authorization: `Bearer ${token}`
            }
          });
        }
      }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const idToken = localStorage.getItem("access_token");
        if (idToken) {
            return this.authService
            .getToken()
            .map(this.setHeaders(req))
            .mergeMap(req =>{console.log("inside mergeMap"); return next.handle(req);})
            .catch(error => {
                if (error.status === 401) {
                    console.log("inside 401");
                return this.authService.updateToken()
                    .map(this.setHeaders(req))
                    .mergeMap(req =>{console.log("inside mergeMap2"); return next.handle(req);});
                }
                return Observable.throw(error);
            });
        }
        else {
            return next.handle(req);
        }
    }
}