import { Component, OnInit, HostBinding, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { PhotoModel } from '../common/models/photo.model';
import { HttpService } from '../services/http.service';

@Component({
  selector: 'add-photo',
  templateUrl: './add-photo.component.html',
  styleUrls: ['./add-photo.component.css']
})
export class AddPhotoComponent implements OnInit {
 
  public showSpinner = false;
  public photo: PhotoModel;  
  public fileToUpload: File = null;
  public createMode: boolean;
  
  constructor(private http: HttpService, private router: Router, public dialogRef: MatDialogRef<AddPhotoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      if(data["photoId"]) {
        this.createMode = false;
      } else {
        this.createMode = true;
        this.photo = new PhotoModel();
      }
  }

  ngOnInit() {
    if(!this.createMode) {
      this.http
      .get<PhotoModel>("api/Photos/GetPhoto/",this.data["photoId"])
      .subscribe(result => {
        this.photo = result.model;
      }, error =>{
         this.dialogRef.close(false);
        });
    }
  }

  public handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  public saveChanges() {
    this.showSpinner = true;
    var id;
    if(this.createMode) {
      this.http
      .postFile<number>("api/Photos/UploadFileIntoDatabase",this.fileToUpload).delay(2000)
      .subscribe(result => {
        if(result.model) {
          this.photo.ImageId = result.model;
          this.http.
          post<number>("api/Photos/SavePhoto",this.photo)
          .subscribe(result => {
            id = result.model;
            this.showSpinner = false;
            this.dialogRef.close(true);
          }, error => {
            this.dialogRef.close(false);
          });
        }
      }, error => {
        this.dialogRef.close(false);
      });    
    } else {
      this.http
      .put<number>("api/Photos/Edit/"+this.photo.id,this.photo).delay(2000)
      .subscribe(result => {
        id = result.model;  
        this.showSpinner = false;
        this.dialogRef.close(true);
      }, error => {
        this.dialogRef.close(false);
      });
    }
  }

  onNoClick() {
    this.dialogRef.close(false);
  }
}
