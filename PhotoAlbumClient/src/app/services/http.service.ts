import {Injectable} from '@angular/core';
import {Jsonp, RequestOptionsArgs} from '@angular/http';
import {HttpClient, HttpResponse, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import { NotifierService } from 'angular-notifier';

import {environment} from '../../environments/environment';
import { JsonResult } from '../common/models/json-result.model';
import { LoginModel } from '../login/models/login.model';
import { AuthService } from './authentication.service';

@Injectable()
export class HttpService {
    private  baseUrl = environment.baseUrl;
    private httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      };
    
    constructor(private http: HttpClient, private authService: AuthService, private readonly notifier: NotifierService, private navigator: Router) {
    }

    get<T> (path: string, params?: string) : Observable<JsonResult<T>> {
       var stringForGet = '';
        if(params){
            stringForGet = params;
        }
        return this.http
        .get<JsonResult<T>>(this.baseUrl + path + stringForGet)
        .map(this.extractData.bind(this))
        .catch(this.handleError);
    }

    post<T> (path: string, param: any, options?: RequestOptionsArgs): Observable<JsonResult<T>> {
        
        const body = JSON.stringify(param);
        return this.http
        .post<JsonResult<T>>(this.baseUrl + path, body, this.httpOptions)
        .map(this.extractData.bind(this))
        .map(this.notify.bind(this))
        .catch(this.handleError);
    }

    put<T> (path: string, param: any, options?: RequestOptionsArgs): Observable<JsonResult<T>> {
        const body = JSON.stringify(param);

        return this.http
        .put<JsonResult<T>>(this.baseUrl + path, body, this.httpOptions)
        .map(this.extractData.bind(this))
        .map(this.notify.bind(this))
        .catch(this.handleError);
    }
    delete<T> (path: string, params?: string) : Observable<JsonResult<T>> {

        return this.http
        .delete<JsonResult<T>>(this.baseUrl + path, this.httpOptions)
        .map(this.extractData.bind(this))
        .map(this.notify.bind(this))
        .catch(this.handleError);
    }

    postFile<T> (path: string, fileToUpload: File): Observable<JsonResult<T>> {
        return this.http
          .post<JsonResult<T>>(this.baseUrl + path, fileToUpload)
          .map(this.extractData.bind(this))          
          .catch(this.handleError);
    }

    private extractData<T> (result: JsonResult<T>) {
        var message = "";
         if (result.hasError) {
            message = result.message;
            if(result.messageDetails && result.messageDetails.length>0) {
                message += ": "+result.messageDetails.join(", ");
            }
            this.notifier.notify( 'error', message );
        }
        return result;
    }

    private notify<T> (result: JsonResult<T>) {
        if (result.model) {
           this.notifier.notify( 'success', 'success');
        }
       return result;
   }
    
    public handleError = (error: HttpErrorResponse) => {
        if(error.status==0) {
            this.navigator.navigate(['/error',error.status, "Server not available"]);
            return Observable.throw("Server not available");
        }
        else if(error.status==401) {
            this.navigator.navigate(['/login']);
        }
        else if(error.error.error == "invalid_grant"){
            return Observable.throw("Update refresh token denied");
        }
        else if(error.status==400){
            this.notifier.notify( 'error', error.error.message );
        }
        else {
            var message = (error.error  && error.error.message) ? error.error.message : "Error has occured";
            this.navigator.navigate(['/error',error.status, message]);
        }
        return Observable.throw(error);
    }
}