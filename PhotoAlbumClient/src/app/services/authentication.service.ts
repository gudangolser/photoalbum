import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse, HttpErrorResponse} from '@angular/common/http';
import {Jsonp, RequestOptionsArgs} from  '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, Subject, BehaviorSubject } from 'rxjs/Rx';

import { NotifierService } from 'angular-notifier';

import {environment} from '../../environments/environment';
import { LoginModel } from '../login/models/login.model';
import { JsonResult } from '../common/models/json-result.model';
import { CookieService } from 'angular2-cookie/services/cookies.service';

@Injectable()
export class AuthService {
    private readonly baseUrl = environment.baseUrl;
    private readonly pathToAuth = 'oauth2/token';
    private refreshTokenCall;

    private httpOptions = {
        headers: new HttpHeaders({
              'Content-Type': 'x-www-form-urlencoded',
              'Audience': 'Any'
        })
      };
    // Observable string sources
    private isLoggedOut = new Subject<boolean>();
    private userName = new Subject<string>();

    // Observable string streams
    loggedOut$ = this.isLoggedOut.asObservable();
    getUserName$ = this.userName.asObservable();

    constructor(private http: HttpClient, private navigator: Router, private cookieService: CookieService, private readonly notifier: NotifierService) {
    }

    login(param: LoginModel): Observable<boolean> {
        var username = param.username;
        var body = "grant_type=" + param.grant_type + "&"
                        + "username=" + param.username + "&"
                        + "password=" + param.password;
        return this.http
        .post(this.baseUrl + this.pathToAuth, body, this.httpOptions)
        .map(this.extractData)
        .catch((error: HttpErrorResponse)=> {
            this.notifier.notify( 'error', error.error.error );
            return Observable.throw(error.error.error);
        });      
    }
    logout() {
        return this.http
        .post(this.baseUrl + "api/Account/Logout", null, this.httpOptions)
        .catch((error: HttpErrorResponse)=> {
            this.notifier.notify( 'error', error.error.error );
            return Observable.throw(error.error.error);
        })
        .subscribe(res =>{
            this.removeToken();
            this.cookieService.removeAll();
            this.navigator.navigate(['/login']);
        });
    }
    removeToken() {
        localStorage.removeItem('access_token');
        localStorage.removeItem('username');
        localStorage.removeItem('refresh_token');
        localStorage.removeItem('expTime');
        this.updateInfoAboutLogin(false);
    }

    checkLogin() : boolean {
        var login = (!!localStorage.getItem('access_token'));
        if(login) {
            this.updateInfoAboutLogin(true);
        } else {
            this.removeToken();
        }
        return login;
    }
    getToken() {
        if (localStorage.getItem('access_token')) {
          return Observable.of(localStorage.getItem('access_token'));
        }   
        return this.updateToken();
    }

    updateToken() {
        if (!this.refreshTokenCall) {
            var body = "grant_type=" + "refresh_token" + "&"
                        + "refresh_token=" + localStorage.getItem('refresh_token');
            this.refreshTokenCall = this.http
            .post(this.baseUrl + this.pathToAuth, body, this.httpOptions)
            .map(this.extractData)
            .catch((error: HttpErrorResponse)=> {
                this.removeToken();
                this.navigator.navigate(['/login']);
                return Observable.throw(error);
            })
            .finally(() => this.refreshTokenCall = null);
        }
        return this.refreshTokenCall;
    }

    private extractData = (result: Object) => {
        var token = result["access_token"];
        if (token) {
            localStorage.setItem('access_token', token);
            localStorage.setItem('refresh_token', result["refresh_token"]);
            localStorage.setItem('expTime', result[".expires"]);
            localStorage.setItem('username', result["userName"]);
            this.updateInfoAboutLogin(true);
            return token;
        }
        else
        console.log("update token false");
            return "";
    }

    getExpirationTime(): number {
        var dateStr = localStorage.getItem('expTime');
        if(dateStr)
            return new Date(dateStr).getTime();
        return 0;
    }

    updateInfoAboutLogin(login: boolean) {
        if(login) {
            this.isLoggedOut.next(false);
            this.userName.next(localStorage.getItem("username"));
        } else {
            this.isLoggedOut.next(true);
            this.userName.next("");
        }
    }
}