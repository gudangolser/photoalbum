import { Component, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { environment } from '../../environments/environment';
import { LoginModel } from './models/login.model';
import { AuthService } from './../services/authentication.service';
import { HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'login',
  templateUrl: './login.component.html'
})
export class LoginComponent {
  public model: LoginModel;
  private  baseUrl = environment.baseUrl;
  public success: boolean;

  constructor(private http: AuthService, private router: ActivatedRoute, private navigator: Router) {
    this.model = new LoginModel();
  }
  public Login(item: LoginModel) {
    this.http.login(this.model).subscribe(result => {
      this.success = result;
      if(this.success)
        this.navigator.navigate(['all-photos']);
    });
  }
  public Registration() {
    this.navigator.navigate(['registration']);
  }
}