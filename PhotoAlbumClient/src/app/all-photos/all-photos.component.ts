import { Component, Inject, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";

import {environment} from './../../environments/environment';
import {PhotoModel} from './../common/models/photo.model';
import { HttpService } from './../services/http.service';
import { PageInfoModel } from '../common/models/page-info.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'all-photos',
  templateUrl: './all-photos.component.html',
  styleUrls: ['./all-photos.component.css']
})
export class AllPhotosComponent implements OnInit{
  private  baseUrl = environment.baseUrl;
  public list: PhotoModel[];
  public pageInfo: PageInfoModel;
  public pages: number;

  public userName: string;
  public firstName: string;
  
  constructor(private http: HttpService, private router: ActivatedRoute) {
    this.pageInfo = new PageInfoModel();
    this.pageInfo.typeOfOrder = 1;
    this.pageInfo.pageNumber = 1;
  }
  
  ngOnInit(): void {
    this.router.params.subscribe(params => {
     this.userName = params['username'];
     if(this.userName){
      this.http
      .get<string>("api/User/"+this.userName+"/GetName")
      .subscribe(result => {
        this.firstName = result.model;
      }, error => console.error(error));
    }
     this.getPhotosInOrder();
    });
  }

  public changeOrder(type: number) {
    this.pageInfo.typeOfOrder = type;
    this.pageInfo.pageNumber = 1;
    this.getPhotosInOrder();
  }

  private getPhotosInOrder() {
    this.getNumberOfPages();
    var path = "";
    if(this.userName){
      path = "api/Photos/"+this.userName;
    } else {
      path = 'api/Photos/List';
    }
    var params = "?";
    for(var param in this.pageInfo) {
      params+=param+"="+this.pageInfo[param]+"&";
    }
    params = params.substr(0,params.length-1);
    this.http
    .get<PhotoModel[]>(path,params)
    .subscribe(result => {
      this.list = result.model;
    }, error => console.error(error));
  }

  public selectPage(page: number) {
    this.pageInfo.pageNumber = page;
    this.getPhotosInOrder();
  }
  
  private getNumberOfPages(): number {
    this.http
    .get<number>('api/Photos/GetCountOfPages'+(this.userName ? "?username="+this.userName : ""))
    .subscribe(result => {
      this.pages =  result.model;
    }, error => console.error(error));
    return 0;
  }
}