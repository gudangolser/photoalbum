import { Component, OnInit } from '@angular/core';
import { PhotoModel } from '../common/models/photo.model';
import { HttpService } from '../services/http.service';
import { ActivatedRoute } from '@angular/router';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { PageInfoModel } from '../common/models/page-info.model';
import { AddPhotoComponent } from '../add-photo/add-photo.component';

@Component({
  selector: 'my-photos',
  templateUrl: './my-photos.component.html',
  styleUrls: ['./my-photos.component.css']
})
export class MyPhotosComponent implements OnInit {
  public list: PhotoModel[];
  private userName: string;
  public pageInfo: PageInfoModel;
  public pages: number;

  constructor(private http: HttpService, private router: ActivatedRoute, public dialog: MatDialog) {
    this.pageInfo = new PageInfoModel();
    this.pageInfo.typeOfOrder = 1;
    this.pageInfo.pageNumber = 1;
  }
  
  ngOnInit() {
    this.getPhotosInOrder();
  }

  private getPhotosInOrder() {
    var strOfParams = "?";
    for(var param in this.pageInfo){
      strOfParams+=param+"="+this.pageInfo[param]+"&";
    }
    strOfParams = strOfParams.substr(0,strOfParams.length-1);
    this.router.params.subscribe(params => {
      this.userName = params['username'];
      if(this.userName) {
        this.getNumberOfPages();
        this.http.get<PhotoModel[]>("api/Photos/"+this.userName+"/Manage",strOfParams)
          .subscribe(result=>{
              this.list = result.model;
          }, error => console.error(error));
      }
    });
  }

  public changeOrder(type: number) {
    this.pageInfo.typeOfOrder = type;
    this.pageInfo.pageNumber = 1;
    this.getPhotosInOrder();
  }

  public selectPage(page: number) {
    this.pageInfo.pageNumber = page;
    this.getPhotosInOrder();
  }

  private getNumberOfPages(): number {
    this.http
    .get<number>('api/Photos/GetCountOfPages',"?username="+this.userName)
    .subscribe(result => {
      this.pages =  result.model;
    }, error => console.error(error));
    return 0;
  }

  public addPhoto() {
    let dialogRef = this.dialog.open(AddPhotoComponent, {
      width: '400pt',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.getPhotosInOrder();
      }
    });
  }
}
