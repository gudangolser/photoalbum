import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { MatButtonModule,
         MatCheckboxModule,
         MatInputModule,
         MatFormFieldModule,
         MatProgressSpinnerModule,
         MatCardModule,
         MatRadioModule,
         MatSliderModule,
         MatSlideToggleModule,
         MatDividerModule} from '@angular/material';
import { MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';
import { CookieService } from 'angular2-cookie/services/cookies.service';

import { NotifierModule, NotifierService } from 'angular-notifier';
import { StarRatingModule } from 'angular-star-rating';

import { AppComponent } from './app.component';
import { LoginComponent } from "./login/login.component";
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { AllPhotosComponent } from './all-photos/all-photos.component';
import { RegistrationComponent } from './registration/registration.component';
import { ErrorComponent } from './error/error.component';
import { HttpService } from './services/http.service';
import { AddPhotoComponent } from './add-photo/add-photo.component';
import { ViewPhotoComponent } from './view-photo/view-photo.component';
import { AuthService } from './services/authentication.service';
import { AuthInterceptor } from './common/interceptors/auth.interceptor';
import { AuthGuard } from './guards/auth.guarg';
import { ProfileComponent } from './profile/profile.component';
import { MyPhotosComponent } from './my-photos/my-photos.component';
import { OrderComponent } from './common/components/order/order.component';
import { PagingComponent } from './common/components/paging/paging.component';
import { LoginGuard } from './guards/login.guard';
import { WindowComponent } from './common/components/confirm-window/confirm-window.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavMenuComponent,
    AllPhotosComponent,
    RegistrationComponent,
    ErrorComponent,
    AddPhotoComponent,
    ViewPhotoComponent,
    ProfileComponent,
    MyPhotosComponent,
    OrderComponent,
    PagingComponent,
    WindowComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    NotifierModule,
    BrowserAnimationsModule,
    MatButtonModule, 
    MatCardModule,
    MatCheckboxModule,
    MatDialogModule,
    MatDividerModule,
    MatInputModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSliderModule,
    MatSlideToggleModule,
    StarRatingModule.forRoot(),
    RouterModule.forRoot([
       {path: '', component: AllPhotosComponent, pathMatch: 'full'},
       {path: 'login', component: LoginComponent, canActivate: [LoginGuard]},
       {path: 'all-photos', component: AllPhotosComponent},
       {path: 'registration', component: RegistrationComponent, canActivate: [LoginGuard]},
       {path: 'add-photo', component: AddPhotoComponent, canActivate: [AuthGuard]},
       {path: ':username', component: AllPhotosComponent},
       {path: ':username/profile', component: ProfileComponent, canActivate: [AuthGuard]},
       {path: ':username/manage', component: MyPhotosComponent, canActivate: [AuthGuard]},
       {path: 'error/:status/:description', component: ErrorComponent},
       {path: '**', component: AllPhotosComponent},
    ])
  ],
  providers: [
    AuthGuard,
    LoginGuard,
    HttpService,
    AuthService,
    CookieService,
    NotifierService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS, 
      useValue: {hasBackdrop: true}
    }
  ],
  entryComponents: [
    WindowComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
